<?php

function js(string $fileName):string
{
    return DIR_JS . $fileName . '.js' ;
}

function css(string $fileName):string
{
    return DIR_CSS . $fileName . '.css' ;
}

function img(string $path):string
{
    return DIR_IMG . $path ;
}

function config(string $fileName):array
{
    return include DIR_CONFIG . $fileName . '.php';
}

function app(string $key):string
{
    $app = config('app');
    return $app[$key];
}

// path: 'fileName.key'
function lang(string $path):string
{
    $explode = explode('.',$path);
    $key = array_pop($explode);
    $fileName = current($explode);
    $fullPath =  getLangPath($fileName);
    $lang = include $fullPath;
    if(!array_key_exists($key,$lang))
        die(MSG_LANG_NOT_EXIST);
    return $lang[$key];
}

function getLangPath(string $fileName):string
{
    return DIR_LANG . app('lang') . '/' . $fileName . '.php';
}

// 1000000 -> 1,000,000
function price2Char(int $price):string
{
    $count = strlen($price)%3;
    $char = substr($price,0,$count);
    for($i=$count; $i<strlen($price); $i+=3){
        if(strlen($char)>0)
            $char .= ',';
        $char .= substr($price,$i,3);
    }
    return $char;
}

function generateCode():string
{
    return (string) rand(10000,20000);
}

function view(string $viewName,array $data = [],string $layout = null):string
{
    $viewName = str_replace(".","/",$viewName);
    $fullViewPath = getViewPath($viewName);
    if(!pathExists($fullViewPath))
        return MSG_VIEW_NOT_EXIST;
    if(sizeof($data)>0)
        extract($data);
    ob_start();
    include $fullViewPath;
    $view = ob_get_clean();
    // view
    if(is_null($layout))
        return $view;
    // layout
    $layout = str_replace(".","/",$layout);
    $fullLayoutPath = getLayoutPath($layout);
    if(!pathExists($fullLayoutPath))
        return MSG_LAYOUT_NOT_EXIST;
    ob_start();
    include $fullLayoutPath;
    return ob_get_clean();
}

function getViewPath(string $viewName):string
{
    return DIR_VIEW_TMP . $viewName . ".php";
}

function getLayoutPath(string $layout):string
{
    return DIR_LAYOUT . $layout . ".php";
}

function getDevice(string $userAgent):string
{
    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$userAgent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($userAgent,0,4)))
        return 'mobile';
    return 'pc';
}

function pathExists(string $path):bool
{
    if(!file_exists($path) or !is_readable($path))
        return false;
    return true;
}

function dd(... $input):void
{
    die(var_dump(... $input));
}

function str2Hex(string $string):string
{
    $hex='';
    for ($i=0; $i < strlen($string); $i++){
        $hex .= dechex(ord($string[$i]));
    }
    return $hex;
}

function E403(string $msg):string
{
    header('HTTP/1.0 403 Forbidden');
    return $msg;
}

function E404():void
{
    header('HTTP/1.0 404 Not Found');
    die(view('404'));
}

function siteUrl($route = ''):string
{
    return BASE_URL.$route;
}

function csrf_token():string
{
    $csrf = $_SESSION['csrf'] ?? null;
    if(!is_null($csrf))
        return $csrf;
    $csrf = bin2hex(random_bytes(16));
    $_SESSION['csrf'] = $csrf;
    return $csrf;
}

function equalStr(string $str1, string $str2):bool
{
    return $str1 == $str2;
}

function addSession(array $items):void
{
    foreach ($items as $key => $val)
        $_SESSION[$key] = $val;
}

function getSession(string $key):string
{
    return $_SESSION[$key] ?? '';
}

function delSession(string $key):void
{
    unset($_SESSION[$key]);
}

function addCookie(string $key,string $value,int $expireHour)
{
    setcookie($key,$value,date('U')+3600*$expireHour);
}

function delCookie(string $key):void
{
    setcookie($key,null,0);
}

function getCookie(string $key) : string
{
    return $_COOKIE[$key] ?? '';
}


