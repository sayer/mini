<?php

namespace App\Middleware;


use App\Core\Request;

abstract class baseMiddleware
{
    public abstract static function handle(Request $request);
}