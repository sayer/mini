<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModelEloquent extends Model
{
	protected $table;
	protected $primaryKey = 'id';
    protected $guarded = ['id'];
}