<?php

namespace App\Models;


class User extends BaseModel
{

    protected $table = 'users';
    protected $guarded = ['id','role'];

    public function userMeta()
    {
        $this->hasMany(UserMeta::class);
    }
    
}