<?php

namespace App\Models;


class UserMeta extends BaseModel
{

    protected $table = 'user_meta';

    public function user()
    {
        $this->belongsTo(User::class,'user_id');
    }
    
}