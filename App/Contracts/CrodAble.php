<?php

namespace App\Contracts;


use App\Repositories\BaseRepo;

interface CrodAble
{
    public function find(int $id):BaseRepo;
    public function create(array $items):object;
    public function update(array $items):object;
    public function delete():bool;
    public function count():int;
    public function where(string $column,string $operand,string $value);
    public function select(array $columns = ['*']);
    public function max(string $column);
}