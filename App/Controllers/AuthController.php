<?php

namespace App\Controllers;


use App\Adapters\Token;
use App\Core\Request;
use App\Core\Security;
use App\Repositories\UserRepo;

class AuthController extends BaseController
{


    /* - - - -   check login   - - - - */
    public static function checkLogin(Request $request,array $uriData):string
    {
        /* - - - -   check login   - - - - */
        $token = self::getToken(getSession('login'),getCookie('remember'));
        /* - - - -   not set token  - - - - */
        if(!$token)
            return view('panel.authForm',['error'=>lang('messages.tokenFalse')],'visitor');
        /* - - - -   decode token  - - - - */
        $userToken = (object) Token::decode($token);
        /* - - - -   not set token  - - - - */
        if(!$userToken)
            return view('panel.authForm',['error'=>lang('messages.userTokenFalse')],'visitor');
        /* - - - -   check Csrf Token  - - - - */
        if(!$request->isCsrf($userToken->csrf))
            return view('panel.authForm',['error'=>lang('messages.csrfFalse')],'visitor');
        /* - - - -   get user  - - - - */
        $userRepo = new UserRepo();
        $user = $userRepo->getUser($userToken->userID);
        if(!$user)
            return view('panel.authForm',['error'=>lang('messages.repoFalse')],'visitor');
        /* - - - -   login : show dashboard  - - - - */
        $data = ['user'=>$user];
        return view('panel.dashboard',$data,'panel');
    }

    private static function getToken(string $sessionLogin,string $cookieLogin)
    {
        $token = $sessionLogin;
        if($token == '')
            $token = $cookieLogin;
        if($token == '')
            return false;
        return $token;
    }

    public static function login(Request $request,array $uriData)
    {
        /* - - - -   check request referer   - - - - */
        if(!$request->isReferer($request->servername))
            die(E403(lang('messages.notValidRequest')));
        /* - - - -   check Csrf Token  - - - - */
        if(!$request->isCsrf($request->csrfToken))
            self::ajaxResponse(lang('messages.csrfFalse'));
        self::doLogin($request->username,$request->password,$request->csrfToken);
    }

    private static function doLogin(string $username,string $password,string $csrfToken)
    {
        /* - - - -   check empty inputs   - - - - */
        if(!strlen($username))
            self::ajaxResponse(lang('messages.emptyInput'));
        if(!strlen($password))
            self::ajaxResponse(lang('messages.emptyInput'));
        /* - - - -   get user with username   - - - - */
        $userRepo = new UserRepo();
        $user = $userRepo->findUserByUsername($username);
        /* - - - -   username not find   - - - - */
        if(!$user)
            self::ajaxResponse(lang('messages.repoFalse'));
        /* - - - -   check password   - - - - */
        $hashedReqPassword = Security::hashing($password);
        if($hashedReqPassword != $user->password)
            self::ajaxResponse(lang('messages.passFalse'));
        /* - - - -   ready to login   - - - - */
        /* - - - -   set token   - - - - */
        $data = ['userID'=>$user->id,'csrf'=>$csrfToken];
        $token = Token::encode($data);
        /* - - - -   set sessions   - - - - */
        addSession(['login'=>$token]);
        /* - - - -   set cookie   - - - - */
        addCookie('remember',$token,app('expireRememberCookie'));
        /* - - - -   login true   - - - - */
        self::ajaxResponse(lang('messages.loginTrue'),true,app('panelUri'));
    }

    public static function register(Request $request,array $uriData)
    {
        /* - - - -   check request referer   - - - - */
        if(!$request->isReferer($request->servername))
            die(E403(lang('messages.notValidRequest')));
        /* - - - -   check Csrf Token  - - - - */
        if(!$request->isCsrf($request->csrfToken))
            self::ajaxResponse(lang('messages.csrfFalse'));
        /* - - - -   check equal password & re   - - - - */
        if(!equalStr($request->password,$request->rePassword))
            return self::ajaxResponse(lang('messages.equalPassFalse'));
        /* - - - -   check user exists   - - - - */
        $hashedPassword = Security::hashing($request->password);
        $userRepo = new UserRepo();
        $find = $userRepo->findUserByUsername($request->username);
        /* - - - -   username find   - - - - */
        if($find)
            return self::ajaxResponse(lang('messages.repoFalse'));
        /* - - - -   not exists : create   - - - - */
        $items = [app('usernameField')=>$request->username, 'password'=>$hashedPassword];
        if(!$userRepo->create($items))
            return self::ajaxResponse(lang('messages.repoFalse'));
        /* - - - -   do login   - - - - */
        self::doLogin($request->username,$request->password,$request->csrfToken);
    }

    public static function logout(Request $request,array $uriData):string
    {
        /* - - - -   check request referer   - - - - */
        if(!$request->isReferer($request->servername))
            die(E403(lang('messages.notValidRequest')));
        /* - - - -   do logout   - - - - */
        delSession('login');
        delSession('csrf');
        delCookie('remember');
        header('location: '.BASE_URL.app('panelUri'));
        exit;
    }



}