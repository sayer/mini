<?php

namespace App\Controllers;


use App\Core\Request;

class HomeController
{

    public function homePage(Request $request,array $uriData):string
    {
        // if used uri with data delete command bottom line to access to uri variables!
        // extract($uriData);

        return view('homePage',[],'visitor');
    }

}