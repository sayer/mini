<?php

namespace App\Controllers;


abstract class BaseController
{

    protected static function ajaxResponse(string $msg,bool $refresh = false,string $toUrl = null,int $second = 0)
    {
        if(self::msgValid($msg))
            die(json_encode(['msg'=>urldecode($msg),'refresh'=>$refresh,'tourl'=>$toUrl,'sec'=>$second]));
    }

    private static function msgValid(string $msg):bool
    {
        return strlen($msg);
    }


}
