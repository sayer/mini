<?php

namespace App\Adapters;


use Firebase\JWT\JWT;

class Token
{

    public static function encode(array $items)
    {
        return JWT::encode($items, KEY_TOKEN);
    }

    public static function decode(string $token)
    {
        return JWT::decode($token, KEY_TOKEN, array('HS256'));
    }

}