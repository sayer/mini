<?php

namespace App\Repositories;


use App\Models\User;

class UserRepo extends BaseRepo
{

    protected $model = User::class;

    public function getUser(int $userID)
    {
        $user = $this->model::find($userID);
        return $this->firstOrFalse($user);
    }

    public function findUserByUsername(string $email)
    {
        $user = $this->model::select(['*'])->where(app('usernameField'),'=',$email);
        return $this->firstOrFalse($user);
    }
}