<?php

namespace App\Repositories;


use App\Models\UserMeta;

class UserMetaRepo extends BaseRepo
{

    protected $model = UserMeta::class;

    public function getVisitorID(string $ip):int
    {
        $visitor = $this->select(['user_id'])->where('value','=',$ip);
        // visitor exist, get first
        if($visitor->count()){
            $visitor = $visitor->first();
            return $visitor->user_id;
        }
        // visitor not exist, create
        $maxUserID = $this->max('user_id');
        $userID = $maxUserID +10000;
        $attr = ['user_id'=>$userID,'attr'=>'ip','value'=>$ip];
        $visitor = $this->create($attr);
        return $visitor->user_id;
    }

}