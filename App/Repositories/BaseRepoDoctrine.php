<?php

namespace App\Repositories;


use App\Contracts\CrodAble;

abstract class BaseRepoDoctrine implements CrodAble
{

    private $attributes = null;

    public function find(int $id) : BaseRepo
    {

        return $this;
    }

    public function create(array $items): object
    {

    }

    public function update(array $items): object
    {

    }

    public function delete(): bool
    {

    }

    public function exists(): bool
    {

    }

}