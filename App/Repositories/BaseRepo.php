<?php

namespace App\Repositories;


abstract class BaseRepo extends BaseRepoEloquent
{

    public function firstOrFalse(object $obj)
    {
        if($obj->count())
            return $obj->first();
        return false;
    }

}