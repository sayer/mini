<?php

namespace App\Repositories;


use App\Contracts\CrodAble;

abstract class BaseRepoEloquent implements CrodAble
{

    protected $model;

    public function find(int $id): BaseRepo
    {
        return $this->model::findOrFail($id);
    }

    public function select(array $columns = ['*'])
    {
        return $this->model::select($columns);
    }

    public function where(string $column, string $operand, string $value)
    {
        return $this->model::where($column, $operand, $value);
    }

    public function create(array $items = []) : object
    {
        return $this->model::create($items);
    }

    public function update(array $items) : object
    {
        return $this->attributes::update($items);
    }

    public function delete() : bool
    {
        return $this->attributes::delete();
    }

    public function first() : object
    {
        return $this->model::first();
    }

    public function count() : int
    {
        return $this->model::count();
    }

    public function max(string $column)
    {
        return $this->model::max($column);
    }

}