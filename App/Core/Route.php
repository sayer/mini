<?php

namespace App\Core;


use App\Middleware\allRequestMiddleware;

class Route
{
    // arg[0] : uri , arg[1] : target
    public static function __callStatic($method, $arguments):void
    {
        /* - - - -   not find route in web.php   - - - - */
        if($arguments[0] == '404')
            E404();

        /* - - - -   request   - - - - */
        $request = Request::Instance();

        /* - - - -   uri   - - - - */
        // explode req uri
        $reqUri = self::explodeUri($request->uri);
        // explode route uri
        $routeUri = self::explodeUri($arguments[0]);
        // check req uri = route uri
        if(!self::allowUri($reqUri[1],$routeUri[1]))
            return;
        // check equal count req & route uri
        if(!self::equalCount($reqUri,$routeUri))
            E404();

        /* - - - -   middleware   - - - - */
        self::callMiddleware($request);

        /* - - - -   data   - - - - */
        $data = self::getUriData($reqUri,$routeUri);

        /* - - - -   request method valid   - - - - */
        if(!$request->isMethod($method))
            die(E403('method not valid !'));

        /* - - - -   controller   - - - - */
        list($className,$methodName) = explode('@',$arguments[1]);
        $className = NAMESPACE_CONTROLLER . $className . 'Controller';
        // class not exist
        if(!class_exists($className))
            die('controller class not exist');
        // method not exist
        if(!method_exists($className,$methodName))
            die('controller method not exist');
        // call controller's method
        $view = $className::$methodName($request,$data);

        // use controller response
        die($view);
    }

    private static function explodeUri(string $uri):array
    {
        $uri = str_replace('/','./',$uri);
        return explode('.',$uri);
    }

    private static function allowUri(string $reqUri,string $routeUri):bool
    {
        return $reqUri == $routeUri;
    }

    private static function getUriData(array $reqUri,array $routeUri):array
    {
        $data = [];
        $reqUri = array_slice($reqUri,2);
        $routeUri = array_slice($routeUri,2);
        if(sizeof($routeUri) && strpos($routeUri[0],'{') !== false){
            for ($i=0;$i<sizeof($routeUri);$i++){
                $routeUri[$i] = str_replace('/',null,$routeUri[$i]);
                $routeUri[$i] = str_replace('{',null,$routeUri[$i]);
                $routeUri[$i] = str_replace('}',null,$routeUri[$i]);
                $reqUri[$i] = str_replace('/',null,$reqUri[$i]);
                $data[$routeUri[$i]] = $reqUri[$i];
            }
        }
        return $data;
    }

    private static function equalCount(array $reqUri,array $routeUri):bool
    {
        return sizeof($reqUri) == sizeof($routeUri);
    }

    private static function callMiddleware(Request $request):void
    {
        // TODO: call middleware.
        allRequestMiddleware::handle($request);
    }
}