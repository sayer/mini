<?php

namespace App\Core;


class Security
{

    public static function hashing(string $data):string
    {
        return hash(app('hashAlgo'),app('hashSalt').$data);
    }



}