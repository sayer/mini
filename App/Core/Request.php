<?php

namespace App\Core;


class Request
{
    private $params;
    private $method;
    private static $instance = null;

    private function __construct()
    {
        $this->params = $_REQUEST;
        $this->params['ip'] = $this->ip();
        $this->params['uri'] = $this->getUri();
        $this->params['agent'] = $_SERVER['HTTP_USER_AGENT'];
        $this->params['referer'] = $_SERVER['HTTP_REFERER'] ?? '';
        $this->params['servername'] = $_SERVER['SERVER_NAME'] ?? '';
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
    }

    public static function Instance(): Request
    {
        if(is_null(self::$instance))
            self::$instance = new Request();
        return self::$instance;
    }

    private function getUri() : string
    {
        $uri = urldecode($_SERVER['REQUEST_URI']);
        return strtok($uri,'?');
    }

    private function ip() : string
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            return $_SERVER['REMOTE_ADDR'];
    }

    public function __get($name) : string
    {
        if($this->paramExists($name))
            return $this->getParam($name);
        return '';
    }

    private function paramExists(string $key) : bool
    {
        return array_key_exists($key,$this->params);
    }

    private function getParam(string $key) : string
    {
        return $this->params[$key];
    }

    public function isMethod(string $method):bool
    {
        return $this->method == $method;
    }

    public function isReferer(string $domain):bool
    {
        if($this->referer == '')
            return true;
        return stripos($this->referer, $domain) !== false;
    }

    public function isCsrf(string $token):bool
    {
        return getSession('csrf') == $token;
    }

    public static function redirect(string $route = ''):void
    {
        header("Location: " . siteUrl($route));
        exit;
    }

}