$(document).ready(function () {
    $("form").on("submit",function (event) {
        event.preventDefault();
        var formTag = $(this);
        if(formTag.attr("action").search('Upload')>=0)
            return;
        ajax(formTag.attr("action"),formTag.serialize(),formTag);
    });
});