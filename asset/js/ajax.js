function ajax(url,formData,formTag)
{
    var parent = $('form');
    $.ajax({
        type:'POST',
        url:url,
        data:formData,
        dataType: 'json',
        success:function(data){
            $(formTag).find('.result').slideDown();
            $(formTag).find('.result').html(data.msg);
            if(data.refresh)
                setTimeout(function(){
                    if(data.tourl !== null)
                        window.location.href = data.tourl;
                    else
                        window.location.reload();
                }, data.sec*1000);
        }
    });
}