<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= lang('theme.title') ?></title>
    <script type="text/javascript" src="<?= js('jquery') ?>"></script>
    <script type="text/javascript" src="<?= js('ajax') ?>"></script>
    <script type="text/javascript" src="<?= js('formSubmit') ?>"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="<?= img('icons/favicon.ico') ?>"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= css('lib/bootstrap.min') ?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= css('lib/font-awesome.min') ?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= css('lib/material-design-iconic-font.min') ?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= css('lib/animate') ?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= css('lib/hamburgers.min') ?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= css('lib/animsition.min') ?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= css('lib/select2.min') ?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= css('lib/daterangepicker') ?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= css('util') ?>">
    <link rel="stylesheet" type="text/css" href="<?= css('main') ?>">
    <link rel="stylesheet" type="text/css" href="<?= css('custom') ?>">
    <!--===============================================================================================-->
</head>
<body>

<a href="<?= app('panelUri') ?>"> پنل </a>

<?= $view ?>

</body>
</html>