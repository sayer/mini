
<div class="limiter">

    <div class="container-login50">
        <div class="wrap-login50">
            <form class="login100-form validate-form" method="post" action="<?= BASE_URL ?>login">
					<span class="login100-form-title m-b-49 m-t-20">
						<?= lang('theme.btnLogin') ?>
					</span>

                <input type="hidden" name="csrfToken" value="<?= csrf_token() ?>" >

                <div class="text-right wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
                    <span class="label-input100"><?= lang('theme.txtUsername') ?></span>
                    <input class="input100" type="text" name="username" placeholder="Type your username" required>
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>

                <div class="text-right wrap-input100 validate-input" data-validate="Password is required">
                    <span class="label-input100"><?= lang('theme.txtPassword') ?></span>
                    <input class="input100" type="password" name="password" placeholder="Type your password" required>
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>

                <div class="text-left p-t-8 p-b-31">
                    <a href="<?= BASE_URL ?>forgotPassword">
                        <?= lang('theme.forgotPass') ?>
                    </a>
                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn">
                            <?= lang('theme.btnLogin') ?>
                        </button>
                    </div>
                </div>

                <div class="txt1 text-center p-t-20 result"></div>

                <div class="txt1 text-center p-t-20 p-b-20">
						<span>
                            <?= lang('theme.loginWith') ?>
						</span>
                </div>

                <div class="flex-c-m">
                    <a href="#" class="login100-social-item bg1">
                        <i class="fa fa-facebook"></i>
                    </a>

                    <a href="#" class="login100-social-item bg2">
                        <i class="fa fa-twitter"></i>
                    </a>

                    <a href="#" class="login100-social-item bg3">
                        <i class="fa fa-google"></i>
                    </a>
                </div>
            </form>
        </div>
    </div>

    <div class="container-register50">
        <div class="wrap-register50">
            <form class="register100-form validate-form" method="post" action="<?= BASE_URL ?>register">
					<span class="login100-form-title m-b-49 m-t-20">
						<?= lang('theme.btnRegister') ?>
					</span>

                <input type="hidden" name="csrfToken" value="<?= csrf_token() ?>" >

                <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
                    <span class="label-input100"><?= lang('theme.txtUsername') ?></span>
                    <input class="input100" required type="email" name="username" placeholder="Type your username">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>

                <div class="wrap-input100 validate-input  m-b-23" data-validate="Password is required">
                    <span class="label-input100"><?= lang('theme.txtPassword') ?></span>
                    <input class="input100" required type="password" name="password" placeholder="Type your password">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Repeat Password is required">
                    <span class="label-input100"><?= lang('theme.txtRePassword') ?></span>
                    <input class="input100" required type="password" name="rePassword" placeholder="Repeat your password">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>

                <div class="text-left p-t-8 p-b-31">
                    <a href="#">
                        <?= lang('theme.forgotPass') ?>
                    </a>
                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn">
                            <?= lang('theme.btnRegister') ?>
                        </button>
                    </div>
                </div>

                <div class="txt1 text-center p-t-20 result"></div>

                <div class="txt1 text-center p-t-10 p-b-20">
						<span>
							<?= lang('theme.registerWith') ?>
						</span>
                </div>

                <div class="flex-c-m">
                    <a href="#" class="login100-social-item bg1">
                        <i class="fa fa-facebook"></i>
                    </a>

                    <a href="#" class="login100-social-item bg2">
                        <i class="fa fa-twitter"></i>
                    </a>

                    <a href="#" class="login100-social-item bg3">
                        <i class="fa fa-google"></i>
                    </a>
                </div>

            </form>
        </div>
    </div>

</div>



<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="<?= js('lib.jquery-3.2.1.min') ?>"></script>
<!--===============================================================================================-->
<script src="<?= js('lib.animsition.min') ?>"></script>
<!--===============================================================================================-->
<script src="<?= js('lib.popper') ?>"></script>
<script src="<?= js('lib.bootstrap.min') ?>"></script>
<!--===============================================================================================-->
<script src="<?= js('lib.select2.min') ?>"></script>
<!--===============================================================================================-->
<script src="<?= js('lib.moment.min') ?>"></script>
<script src="<?= js('lib.daterangepicker') ?>"></script>
<!--===============================================================================================-->
<script src="<?= js('lib.countdowntime') ?>"></script>
<!--===============================================================================================-->
<script src="<?= js('main') ?>"></script>


