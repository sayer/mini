<?php

session_start();
require_once 'bootstrap/constants/web.php';
require_once 'vendor/autoload.php';
require_once 'helper/helpers.php';
require_once 'bootstrap/init.php';
require_once 'routes/web.php';
require_once 'routes/app.php';
