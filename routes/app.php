<?php

use App\Core\Route;

// Auth
Route::get('/'.app('panelUri'),'Auth@checkLogin');
Route::get('/logout','Auth@logout');
Route::post('/register','Auth@register');
Route::post('/login','Auth@login');

// E404
Route::get('404');