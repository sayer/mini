<?php

$inputFalse = 'مقادیر ارسالی اشتباه می باشند !';
$processFalse = 'پردازش با خطا مواجه شده است !';
$loginFalse = 'هنگام ورود خطایی رخ داده است !';
$registerFalse = 'هنگام ثبت نام خطایی رخ داده است !';

return [
    'notValidRequest'=>'درخواست شما مجاز نمی باشد !',
    'tokenFalse'=>$processFalse,
    'userTokenFalse'=>$processFalse,
    'csrfFalse'=>$inputFalse,
    'repoFalse'=>$processFalse,
    'loginTrue'=>'با موفقیت وارد شدید . درحال انتقال به پنل ...',
    'emptyInput'=>'لطفا تمام مقادیر را وارد نمایید !',
    'passFalse'=>$loginFalse,
    'equalPassFalse'=>'رمزعبور با تکرار آن همخوانی ندارد !',


];
