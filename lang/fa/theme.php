<?php

return [
    'title'=>'عنوان سایت',
    'title404'=>'صفحه پیدا نشد!',
    'title404'=>'صفحه پیدا نشد!',
    'txtUsername'=>'نام کاربری',
    'txtPassword'=>'رمز عبور',
    'txtRePassword'=>'تکرار رمز عبور',
    'btnRegister'=>'ثبت نام',
    'btnLogin'=>'ورود',
    'loginWith'=>'یا وارد شوید با استفاده از : ',
    'registerWith'=>'یا ثبت نام کنید با استفاده از : ',
    'forgotPass'=>'رمز عبور را فراموش کرده اید ؟',
];