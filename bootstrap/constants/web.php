<?php

const NAMESPACE_CONTROLLER = 'App\Controllers\\';

const BASE_URL = 'http://shop.hs/';
const DIR_VIEW = 'view/';
const DIR_LAYOUT = DIR_VIEW . 'layout/';
const DIR_VIEW_TMP = DIR_VIEW . 'tmp/';
const DIR_ASSET = BASE_URL . 'asset/';
const DIR_IMG = DIR_ASSET . 'img/';
const DIR_CSS = DIR_ASSET . 'css/';
const DIR_JS = DIR_ASSET . 'js/';
const DIR_LANG = 'lang/';
const DIR_CONFIG = 'config/';

const MSG_LANG_NOT_EXIST = 'lang not exist';
const MSG_VIEW_NOT_EXIST = 'View Not exists !';
const MSG_LAYOUT_NOT_EXIST = 'Layout Not exists !';

const KEY_TOKEN = 'sayer_xS;TQ)>rEn/b!&+bC),R>SQ`HK>1#KKr3fB/d8C!\'3p8oy@EOh0?p?MS]JKw>VLG%7';
const KEY_ENCRYPT = 'sayer_=_6dx1w\'AX0~X91\'P7h*;@rJFX6H8YUl{8Lqi/(K/-WkA#Cew!Mh<8_\']FFvDqb';
